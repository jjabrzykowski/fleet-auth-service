FROM node:10

LABEL maintainer="Kamil Karczmarczyk <k.karczmarczyk@futuremind.com>"

WORKDIR /usr/src/app

ARG npm_url
ARG npm_token
ENV FM_NPM_PROXY=$npm_url
ENV FM_NPM_TOKEN=$npm_token

COPY package*.json ./

COPY npmrc_config .npmrc

RUN npm install
# RUN npm install --only=production

COPY . .

RUN npm run build

EXPOSE 8080

CMD [ "npm", "start" ]
