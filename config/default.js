module.exports = {
  app: {
    port: 9006,
    host: '0.0.0.0',
    powerbi: {
      reports: {
        WORK_PERSPECTIVE: 'f3f80c17-77f2-4dd0-83fd-30af98535bb8',
        VEHICLE_DETAILS: '1ebb372a-d7a1-40ef-8681-4e9338964c13',
        MAIN_DASHBOARD: '69e0da39-78db-48c3-99a4-0e0e56a48751',
      }
    }
  },
  jwt: {
    secret: 'FleetConnectJwtKey',
    algorithm: 'HS256',
    expiration: '15 days',
    refresh: {
      secret: 'FleetConnectJwtRefreshKey',
      algorithm: 'HS256',
      expiration: '30 days',
    },
  },
  typeorm: {
    host: 'puresoft-test.database.windows.net',
    port: 1433,
    database: 'fleetconnect-relations',
    username: 'migration',
    password: 'SuperSecret!',
  },
  good: {
    ops: {
      interval: 1000,
    },
    reporters: {
      console: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{log: '*', request: '*', response: '*'}],
      }, {
        module: 'good-console',
      }, 'stdout'],
    },
    includes: {
      request: ['headers', 'payload'],
      response: ['payload']
    },
  },
  hapiSwagger: {
    debug: true,
    grouping: 'tags',
    info: {
      title: 'Fleet Connect API auto-generated docs',
    },
  },
};
