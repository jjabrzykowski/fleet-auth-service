import * as config from 'config';
import * as core from '@fm-fleet-core/core-service';
import User = core.domain.entities.User;
import { Repository, getRepository } from 'typeorm';
import { EntityNotFoundError, ReportNotFoundError } from '../../errors';
import { IReportToken, reportAccessToken } from '../../infrastructure/azure/powerbi';

export async function createToken(userId: string, reportId: string): Promise<IReportToken> {
  const userRepository: Repository<User> = getRepository(User);

  const user: User = await userRepository.findOne(userId, {
    relations: ['client'],
  });

  if (!user) {
    throw new EntityNotFoundError();
  }

  const reportUuid: string = translateReportId(reportId);
  const workspaceId: string = '1107d9b5-0102-477e-aac0-79ce6bf50682';

  return reportAccessToken(workspaceId, reportUuid, user.client.id);
}

function translateReportId(reportId: string): string {
  if (!config.has(`app.powerbi.reports.${reportId}`)) {
    throw new ReportNotFoundError({ reportId });
  }

  return config.get<string>(`app.powerbi.reports.${reportId}`);
}
