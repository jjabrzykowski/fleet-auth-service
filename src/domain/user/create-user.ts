import { domain } from '@fm-fleet-core/core-service';
import User = domain.entities.User;
import { generate } from 'shortid';
import { getRepository, Repository } from 'typeorm';
import { hash } from '../auth/password';
import { EntityAlreadyExistsError } from '../../errors';

export async function createUser(payload: Partial<User>): Promise<User> {
  const userRepository: Repository<User> = getRepository(User);

  const [loginCount, emailCount]: [number, number] = await Promise.all([
    userRepository.count({
      where: { login: payload.login },
    }),
    userRepository.count({
      where: { email: payload.email },
    }),
  ]);

  if (loginCount > 0) {
    throw new EntityAlreadyExistsError({
      entity: 'User', context: { login: payload.login },
    });
  }

  if (emailCount > 0) {
    throw new EntityAlreadyExistsError({
      entity: 'User', context: { email: payload.email },
    });
  }

  const user: User = userRepository.create(payload);

  user.id = generate();
  user.password = await hash(payload.password);
  user.refreshTokenSalt = generate();

  return userRepository.save(user);
}
