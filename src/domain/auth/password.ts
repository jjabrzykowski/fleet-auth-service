import * as bcrypt from 'bcryptjs';

export async function check(password: string, hashedPassword: string): Promise<boolean> {
  return password ? bcrypt.compare(password, hashedPassword) : Promise.resolve(false);
}

export async function hash(password: string): Promise<string> {
  return bcrypt.hash(password, 10);
}
