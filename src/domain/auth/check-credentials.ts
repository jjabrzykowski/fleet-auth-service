import * as core from '@fm-fleet-core/core-service';
import User = core.domain.entities.User;
import { Repository, getRepository } from 'typeorm';
import { check } from './password';

export async function checkCredentials(login: string, password: string): Promise<User> {
  const userRepository: Repository<User> = getRepository(User);

  const user: User = await userRepository.findOne({ login: login }, {
    relations: ['roles', 'roles.abilities'],
    loadRelationIds: {
      relations: ['client'],
      disableMixedMap: true,
    },
  });

  if (!user || !await check(password, user.password)) {
    return Promise.reject();
  }

  return user;
}
