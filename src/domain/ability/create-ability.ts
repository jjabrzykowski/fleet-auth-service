import { domain } from '@fm-fleet-core/core-service';
import Ability = domain.entities.Ability;
import { getRepository, Repository } from 'typeorm';
import { generate } from 'shortid';
import { EntityAlreadyExistsError } from '../../errors';

export async function createAbility(payload: Partial<Ability>): Promise<Ability> {
  const repository: Repository<Ability> = getRepository(Ability);

  const abilityCount: number = await repository.count({
    where: { entity: payload.entity, action: payload.action },
  });

  if (abilityCount > 0) {
    throw new EntityAlreadyExistsError({
      entity: 'Ability', context: {
        entity: payload.entity,
        action: payload.action,
      },
    });
  }

  const ability: Ability = new Ability();

  ability.id = generate();
  ability.entity = payload.entity;
  ability.action = payload.action;
  ability.name = payload.name;

  return repository.save(ability);
}
