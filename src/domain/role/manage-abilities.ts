import { domain } from '@fm-fleet-core/core-service';
import Role = domain.entities.Role;
import Ability = domain.entities.Ability;
import { getRepository, Repository } from 'typeorm';

export async function setAbilities(role: Role, abilities: Ability[]): Promise<Role> {
  const repository: Repository<Role> = getRepository(Role);

  role.abilities = abilities;

  return repository.save(role);
}
