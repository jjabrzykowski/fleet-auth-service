import { domain } from '@fm-fleet-core/core-service';
import Role = domain.entities.Role;
import { getRepository, Repository } from 'typeorm';
import { generate } from 'shortid';
import { EntityAlreadyExistsError } from '../../errors';

export async function createRole(payload: Partial<Role>): Promise<Role> {
  const repository: Repository<Role> = getRepository(Role);

  const nameCount: number = await repository.count({
    where: { name: payload.name },
  });

  if (nameCount > 0) {
    throw new EntityAlreadyExistsError({
      entity: 'Role', context: { name: payload.name },
    });
  }

  const role: Role = new Role();

  role.id = generate();
  role.name = payload.name;

  return repository.save(role);
}
