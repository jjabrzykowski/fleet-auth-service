// tslint:disable:max-classes-per-file

abstract class CustomError extends Error {
  public name: string;
  public message: string;
  public stack?: string;
  public isCustom?: boolean;
  public code?: string;
  public data?: object;
  public httpCode?: number;

  protected constructor(code: string, message: string, data: object, httpCode: number = 500) {
    super(message);

    this.name = this.constructor.name;
    this.code = code;
    this.message = message;
    this.stack = new Error().stack;
    this.isCustom = true;
    this.data = data || {};
    this.httpCode = httpCode;
  }
}

export class WrongCredentialsError extends CustomError {
  constructor(data?: object) {
    super('WRONG_CREDENTIALS', 'Provided login or password are wrong!', data, 403);
  }
}

export class EntityAlreadyExistsError extends CustomError {
  constructor(data?: object) {
    super('ENTITY_ALREADY_EXISTS', 'Entity already exists!', data, 400);
  }
}

export class EntityNotFoundError extends CustomError {
  constructor(data?: object) {
    super('ENTITY_NOT_FOUND', 'Entity not found!', data, 404);
  }
}

export class ReportNotFoundError extends CustomError {
  constructor(data?: object) {
    super('REPORT_NOT_FOUND', 'Report not found!', data, 404);
  }
}

export class AccountNotActiveError extends CustomError {
  constructor(data?: object) {
    super('ACCOUNT_NOT_ACTIVE', 'ACCOUNT NOT ACTIVE', data, 401);
  }
}

export class AccountNotFoundError extends CustomError {
  constructor(data?: object) {
    super('ACCOUNT_NOT_FOUND', 'ACCOUNT NOT FOUND', data, 401);
  }
}

export class ValidationError extends CustomError {
  constructor(data?: object) {
    super('VALIDATION_ERROR', '', data, 400);
  }
}
export class ServiceUnavailable extends CustomError {
  constructor(data?: object) {
    super('SERVICE_UNAVAILABLE', '', data, 500);
  }
}

export class ForbiddenError extends CustomError {
  constructor(data?: object) {
    super('FORBIDDEN', 'You do not have access to this endpoint', data, 403);
  }
}
