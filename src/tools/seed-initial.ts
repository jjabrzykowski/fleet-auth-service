import { connect } from '../infrastructure/typeorm';
import { createUser } from '../domain/user/create-user';
import { logger } from '../logger';
import { createRole } from '../domain/role/create-role';
import { domain } from '@fm-fleet-core/core-service';
import Role = domain.entities.Role;
import Ability = domain.entities.Ability;
import { createAbility } from '../domain/ability/create-ability';
import { setAbilities } from '../domain/role/manage-abilities';

const usersPayload: object[] = [
  {
    firstName: 'Rafał',
    lastName: 'Dziędzioł',
    email: 'r.dziedziol@futuremind.com',
    login: 'rdziedziol',
  },
  {
    firstName: 'Kamil',
    lastName: 'Karczmarczyk',
    email: 'k.karczmarczyk@futuremind.com',
    login: 'kkarczmarczyk',
  },
  {
    firstName: 'Łukasz',
    lastName: 'Grzywa',
    email: 'l.grzywa@futuremind.com',
    login: 'lgrzywa',
  },
  {
    firstName: 'Paweł',
    lastName: 'Josiek',
    email: 'p.josiek@futuremind.com',
    login: 'pjosiek',
  },
  {
    firstName: 'Jakub',
    lastName: 'Jałbrzykowski',
    email: 'jakub.jalbrzykowski@gmail.com',
    login: 'jjalbrzykowski',
  },
];

async function createSuperAdminRole(): Promise<Role> {
  try {
    const role: Role = await createRole({ name: 'SuperAdmin' });
    const ability: Ability = await createAbility({
      name: 'SuperAdmin',
      entity: 'system',
      action: 'superadmin',
    });

    await setAbilities(role, [ability]);
    return role;
  } catch (e) {
    logger.debug(e.message);
  }
}

(async (): Promise<void> => {
  await connect();

  const role: Role = await createSuperAdminRole();

  await Promise.all(usersPayload.map(async (payload) => {
    try {
      await createUser({
        ...payload,
        password: 'qwerty123',
        settings: {},
        roles: [role],
      });
    } catch (e) {
      logger.debug(e.message);
    }

    return Promise.resolve();
  }));
})();
