import * as Hapi from 'hapi';
import { createServer } from './infrastructure/hapi/server';
import * as core from '@fm-fleet-core/core-service';
import { connect } from './infrastructure/typeorm';

(async (): Promise<void> => {
  const server: Hapi.Server = await createServer();

  try {
    await core.infrastructure.typeorm.migrate();
    server.log('debug', 'DB synced');
    await connect();

    await server.start();
    server.log('debug', `Server running at: ${server.info.uri}`);
  } catch (err) {
    server.log('error', err);
  }
})();

export const ROOT_DIR: string = __dirname;

export const version: string = require(`../package.json`).version; // tslint:disable-line
