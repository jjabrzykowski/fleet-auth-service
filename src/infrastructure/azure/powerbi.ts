import * as querystring from 'querystring';
import * as Wreck from 'wreck';
import { IncomingMessage } from 'http';

interface IActiveDirectoryLoginResponse {
  token_type: string;
  scope: string;
  expires_in: string;
  ext_expires_in: string;
  expires_on: string;
  not_before: string;
  resource: string;
  access_token: string;
  refresh_token: string;
  id_token: string;
}

interface IGenerateTokenResponse {
  '@odata.context': string;
  token: string;
  tokenId: string;
  expiration: Date;
}

interface IReportInfoResponse {
  '@odata.context': string;
  id: string;
  name: string;
  webUrl: string;
  embedUrl: string;
  isOwnedByMe: boolean;
  datasetId: string;
  ownerName: string;
}

export interface IReportToken {
  token: string;
  embedUrl: string;
  reportId: string;
}

export async function reportAccessToken(workspaceId: string, reportUuid: string, clientId: string): Promise<IReportToken> {
  const activeDirectoryToken: string = await getActiveDirectoryToken();

  const reportUrl: string = `https://api.powerbi.com/v1.0/myorg/groups/${workspaceId}/reports/${reportUuid}`;

  const reportInfoResponse: { res: IncomingMessage, payload: IReportInfoResponse } = await Wreck.get(reportUrl, {
    headers: {
      Authorization: `Bearer ${activeDirectoryToken}`,
    },
    json: true,
  });

  const generateTokenResponse: { res: IncomingMessage, payload: IGenerateTokenResponse } = await Wreck.post(
    `${reportUrl}/GenerateToken`,
    {
      headers: {
        Authorization: `Bearer ${activeDirectoryToken}`,
      },
      payload: {
        accessLevel: 'view',
        identities: [
          {
            username: clientId, // our clientId
            roles: ['Customer'],
            datasets: [reportInfoResponse.payload.datasetId],
          },
        ],
      },
      json: true,
    },
  );

  return {
    token: generateTokenResponse.payload.token,
    embedUrl: reportInfoResponse.payload.embedUrl,
    reportId: reportInfoResponse.payload.id,
  };
}

async function getActiveDirectoryToken(): Promise<string> {
  const requestBody: string = querystring.stringify({
    grant_type: 'password',
    resource: 'https://analysis.windows.net/powerbi/api',
    client_id: '94a33e9d-375f-411b-be02-61748e13cd8a', // applicationId
    username: 'powerbi.stage@fleetconnect.es',
    password: 'gdGTDH3gujr24.',
    scope: 'openid',
  });

  const { payload }: { res: IncomingMessage, payload: IActiveDirectoryLoginResponse } = await Wreck.post(
    'https://login.windows.net/common/oauth2/token',
    {
      payload: requestBody,
      json: true,
    },
  );

  return payload.access_token;
}
