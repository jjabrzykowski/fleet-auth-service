import { get } from 'config';
import { Connection, createConnection } from 'typeorm';
import * as core from '@fm-fleet-core/core-service';
import entities = core.domain.entities;

export async function connect(): Promise<Connection> {
  return createConnection({
    type: 'mssql',
    host: get<string>('typeorm.host'),
    port: get<number>('typeorm.port'),
    username: get<string>('typeorm.username'),
    password: get<string>('typeorm.password'),
    database: get<string>('typeorm.database'),
    options: {
      encrypt: true,
    },
    entities: Object.keys(entities).map(name => entities[name]),
    synchronize: false,
    logging: true,
  });
}
