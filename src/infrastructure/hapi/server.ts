import * as config from 'config';
import * as Hapi from 'hapi';
import { routes } from '../../app/routes';
import { options, plugins } from '@fm-fleet-core/hapi';

export async function createServer(): Promise<Hapi.Server> {
  const server: Hapi.Server = new Hapi.Server({
    ...options,
    host: config.get<string>('app.host'),
    port: config.get<number>('app.port'),
  });

  await server.register([
    ...plugins,
  ]);

  server.route(routes);

  return server;
}
