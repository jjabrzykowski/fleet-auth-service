export interface ICreateUser {
  firstName: string;
  lastName: string;
  email: string;
  login: string;
  password: string;
  settings: {
    [key: string]: string;
  };
}
