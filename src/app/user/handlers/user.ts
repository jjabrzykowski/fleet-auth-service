import { Request, ResponseValue } from 'hapi';
import { ICreateUser } from '../interfaces/payload';
import { createUser } from '../../../domain/user/create-user';
import { getRepository, Repository } from 'typeorm';
import { domain } from '@fm-fleet-core/core-service';
import User = domain.entities.User;
import { EntityNotFoundError } from '../../../errors';

export async function creteUserHandler(req: Request): Promise<ResponseValue> {
  const payload: ICreateUser = <ICreateUser>req.payload;

  try {
    return await createUser(payload);
  } catch (e) {
    return e;
  }
}

export async function getUserHandler(req: Request): Promise<ResponseValue> {
  const repository: Repository<User> = getRepository(User);

  try {
    const user: User = await repository.findOne(req.params.userId);

    return user ? user : new EntityNotFoundError();
  } catch (e) {
    return e;
  }
}

export async function getUserSettingsHandler(req: Request): Promise<ResponseValue> {
  const repository: Repository<User> = getRepository(User);

  try {
    const user: User = await repository.findOne(req.params.userId);

    return user ? user.settings : new EntityNotFoundError();
  } catch (e) {
    return e;
  }
}

export async function updateUserSettingsHandler(req: Request): Promise<ResponseValue> {
  const payload: { [key: string]: string } = <{ [key: string]: string }>req.payload;
  const repository: Repository<User> = getRepository(User);

  try {
    const user: User = await repository.findOne(req.params.userId);

    if (!user) {
      return new EntityNotFoundError();
    }

    user.settings = { ...user.settings, ...payload };
    repository.save(user);

    return user.settings;
  } catch (e) {
    return e;
  }
}
