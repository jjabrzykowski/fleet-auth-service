import { ServerRoute } from 'hapi';
import { creteUserHandler, getUserHandler, getUserSettingsHandler, updateUserSettingsHandler } from './handlers/user';

export const routes: ServerRoute[] = [
  {
    path: '/users',
    method: 'POST',
    handler: creteUserHandler,
  },
  {
    path: '/users/{userId}',
    method: 'GET',
    handler: getUserHandler,
  },
  {
    path: '/users/{userId}/settings',
    method: 'GET',
    handler: getUserSettingsHandler,
  },
  {
    path: '/users/{userId}/settings',
    method: 'PATCH',
    handler: updateUserSettingsHandler,
  },
];
