import { ServerRoute } from 'hapi';
import { loginHandler } from './handlers/login';

export const routes: ServerRoute[] = [
  {
    path: '/auth/login',
    method: 'POST',
    handler: loginHandler,
    options: {
      auth: false,
    },
  },
];
