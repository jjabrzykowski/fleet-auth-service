import { Request, ResponseValue } from 'hapi';
import * as core from '@fm-fleet-core/core-service';
import User = core.domain.entities.User;
import { WrongCredentialsError } from '../../../errors';
import { checkCredentials } from '../../../domain/auth/check-credentials';
import { createRefreshToken, createToken } from '../strategy/jwt-public';

interface IPayload {
  username: string;
  password: string;
}

export async function loginHandler(req: Request): Promise<ResponseValue> {
  const payload: IPayload = <IPayload>req.payload;

  let user: User;
  try {
    user = await checkCredentials(payload.username, payload.password);
  } catch (e) {
    return new WrongCredentialsError();
  }

  return {
    token: createToken(user),
    refreshToken: createRefreshToken(user),
  };
}
