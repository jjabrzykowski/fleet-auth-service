import { get } from 'config';
import * as jwt from 'jsonwebtoken';
import { version } from '../../../index';
import * as core from '@fm-fleet-core/core-service';
import User = core.domain.entities.User;
import IJwtToken = core.app.auth.interfaces.IJwtToken;
import { flatten } from 'lodash';
import { DeepPartial } from 'typeorm';

export function createToken(user: User): string {
  const token: IJwtToken = {
    user: { id: user.id, login: user.login, clientId: user.client.id, abilities: extractAbilities(user) },
    aud: user.login,
    iss: `fleet-connect-auth/${version}`,
  };

  return jwt.sign(token, get<string>('jwt.secret'), {
    algorithm: get<string>('jwt.algorithm'),
    expiresIn: get<string>('jwt.expiration'),
  });
}

export function createRefreshToken(user: User): string {
  const token: DeepPartial<IJwtToken> = {
    user: { login: user.login },
    aud: user.login,
    iss: `fleet-connect-auth/${version}`,
  };

  const secret: string = `${get<string>('jwt.refresh.secret')}${user.refreshTokenSalt}`;

  return jwt.sign(token, secret, {
    algorithm: get<string>('jwt.refresh.algorithm'),
    expiresIn: get<string>('jwt.refresh.expiration'),
  });
}

function extractAbilities(user: User): string[] {
  return flatten(
    user.roles.map((role) => role.abilities.map((ability) => `${ability.entity}_${ability.action}`)),
  );
}
