import { ServerRoute } from 'hapi';
import { reportTokenHandler } from './handlers/report-token';
import * as powerbiValidator from './validators/powerbi-token';

export const routes: ServerRoute[] = [
  {
    path: '/powerbi/{userId}/token',
    method: 'POST',
    handler: reportTokenHandler,
    options: {
      validate: powerbiValidator.request,
      // response: {
      //   schema: powerbiValidator.response,
      // },
    },
  },
];
