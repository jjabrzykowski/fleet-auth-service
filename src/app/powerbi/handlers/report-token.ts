import { Request, ResponseValue } from 'hapi';
import { createToken } from '../../../domain/powerbi/create-token';

interface IPayload {
  reportId: string;
}

export async function reportTokenHandler(request: Request): Promise<ResponseValue> {
  const userId: string = request.params.userId;
  const reportId: string = (<IPayload>request.payload).reportId;

  try {
    return await createToken(userId, reportId);
  } catch (e) {
    return e;
  }
}
