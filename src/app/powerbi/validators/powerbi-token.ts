import { object, Schema, string } from 'joi';
import { RouteOptionsValidate } from 'hapi';

export const request: RouteOptionsValidate = {
  payload: {
    reportId: string().required(),
  },
};

export const response: Schema = object({
  token: string().required(),
});
