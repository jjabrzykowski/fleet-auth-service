import { ServerRoute } from 'hapi';
import { routes as authRoutes } from './auth/routes';
import { routes as userRoutes } from './user/routes';
import { routes as powerbiRoutes } from './powerbi/routes';

export const routes: ServerRoute[] = [
  ...authRoutes,
  ...userRoutes,
  ...powerbiRoutes,
];
