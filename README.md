# Node.js typescript boilerplate

This repository is a base project for backend project in typescript (node.js) with proper files. 

Contains:

- tslint config
- typescript compiler config (tsc)
- .editorconfig
- config for nvm/avn
- sample Dockerfile
- bitbucket pipelines sample config
- sample app config
- tests (using facebook's jest)
- changelog configuration with sample

