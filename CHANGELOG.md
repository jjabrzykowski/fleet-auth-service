# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.1.13"></a>
## [1.1.13](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.12...v1.1.13) (2018-11-23)


### Features

* add update error handling and move validators here ([13c4ba6](https://bitbucket.org/futuremind/fleet-auth-service/commits/13c4ba6))



<a name="1.1.12"></a>
## [1.1.12](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.11...v1.1.12) (2018-11-22)


### Features

* add PowerBI reports ([75480bb](https://bitbucket.org/futuremind/fleet-auth-service/commits/75480bb))



<a name="1.1.11"></a>
## [1.1.11](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.10...v1.1.11) (2018-11-22)


### Features

* change PowerBI report ID ([9bd8130](https://bitbucket.org/futuremind/fleet-auth-service/commits/9bd8130))



<a name="1.1.10"></a>
## [1.1.10](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.9...v1.1.10) (2018-11-21)


### Bug Fixes

* change user settings payload ([3b0ed82](https://bitbucket.org/futuremind/fleet-auth-service/commits/3b0ed82))



<a name="1.1.9"></a>
## [1.1.9](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.8...v1.1.9) (2018-11-20)


### Features

* get and update user settings ([81f6448](https://bitbucket.org/futuremind/fleet-auth-service/commits/81f6448))



<a name="1.1.8"></a>
## [1.1.8](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.7...v1.1.8) (2018-11-16)


### Features

* get user endpoint ([5024162](https://bitbucket.org/futuremind/fleet-auth-service/commits/5024162))



<a name="1.1.7"></a>
## [1.1.7](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.6...v1.1.7) (2018-11-16)


### Features

* migrate to mssql ([51bba19](https://bitbucket.org/futuremind/fleet-auth-service/commits/51bba19))



<a name="1.1.6"></a>
## [1.1.6](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.5...v1.1.6) (2018-11-14)


### Features

* generate PowerBI access token - add reportId and embedUrl to response ([34b905e](https://bitbucket.org/futuremind/fleet-auth-service/commits/34b905e))



<a name="1.1.5"></a>
## [1.1.5](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.4...v1.1.5) (2018-11-14)


### Features

* generate PowerBI access token ([80a76b7](https://bitbucket.org/futuremind/fleet-auth-service/commits/80a76b7))



<a name="1.1.4"></a>
## [1.1.4](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.3...v1.1.4) (2018-11-08)


### Features

* generate JWT refresh token ([70e5766](https://bitbucket.org/futuremind/fleet-auth-service/commits/70e5766))



<a name="1.1.3"></a>
## [1.1.3](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.2...v1.1.3) (2018-11-07)


### Bug Fixes

* login payload ([1cf80e5](https://bitbucket.org/futuremind/fleet-auth-service/commits/1cf80e5))


### Features

* creating user, role and ability ([df38936](https://bitbucket.org/futuremind/fleet-auth-service/commits/df38936))
* extended initial seeder ([c5576cc](https://bitbucket.org/futuremind/fleet-auth-service/commits/c5576cc))



<a name="1.1.2"></a>
## [1.1.2](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.1...v1.1.2) (2018-11-06)


### Bug Fixes

* ssl support for postgres ([d8edf76](https://bitbucket.org/futuremind/fleet-auth-service/commits/d8edf76))



<a name="1.1.1"></a>
## [1.1.1](https://bitbucket.org/futuremind/fleet-auth-service/compare/v1.1.0...v1.1.1) (2018-11-06)



<a name="1.1.0"></a>
# 1.1.0 (2018-11-06)


### Features

* login handler ([d1da3fe](https://bitbucket.org/futuremind/fleet-auth-service/commits/d1da3fe))



<a name="1.0.1"></a>
## 1.0.1 (2018-10-22)
